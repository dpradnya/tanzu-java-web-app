package com.example.springboot;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

	@RequestMapping("/")
	public String index() {
		//return "Greetings from Spring Boot + Tanzu!"; v1.2
		return "Greetings from ALM TEAM -v1.3"; // v1.3 
	}

}